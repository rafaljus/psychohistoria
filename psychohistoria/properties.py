YEARS_RANGE = xrange(2011, 2016)
FUTURE_YEARS_RANGE = xrange(2016, 2018)
ATTRIBUTES = [
    'is_employed', 'is_disabled', 'is_female', 'has_been_convicted',
    'is_addicted_to_alcohol', 'is_a_smoker', 'has_heart_issues',
    'is_addicted_to_drugs', 'is_active', 'healthy_food', 'has_cancer',
    'is_homeless', 'education', 'happiness', 'government_support', 'age',
    'number_of_children', 'size_of_house', 'minutes_of_commuting',
    'salary', 'distance_from_health_care', 'health_index'
]
