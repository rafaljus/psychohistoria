class ParagraphBuilder:
    def build(self, attribute, value, max_val=None):
        if type(value) == type(True):
            value = 'tak' if value else 'nie'
        if max_val:
            return '<p><strong>{}:</strong> {}/{}</p>'.format(attribute, value, max_val)
        else:
            return '<p><strong>{}:</strong> {}</p>'.format(attribute, value)