from calculator.calc import SingleCountryCalculator, TwoCountriesCalculator
from calculator.predictor import Predictor
from database_manager.output_getters import get_mean_data_about_country
from database_manager.output_models import init_and_get_output_country_data_from_tuples
from psychohistoria.properties import YEARS_RANGE, FUTURE_YEARS_RANGE, ATTRIBUTES


class CountryDataPack:
    year_packs = dict()
    country = None

    def __init__(self, country):
        self.country = country

        # Dane pobrane
        for year in YEARS_RANGE:
            new_data_pack = CountryYearlyDataPack(self.country, year)
            setattr(self, 'year' + str(year), new_data_pack)
            self.year_packs[year] = new_data_pack

        # Wartosci przewidywane
        for year in FUTURE_YEARS_RANGE:
            new_data_pack = CountryYearlyPredictedDataPack(self.year_packs, year)
            setattr(self, 'year' + str(year), new_data_pack)


class CountryYearlyDataPack:
    def __init__(self, country, year):
        self.year = year
        self.calculated_mean_data = get_mean_data_about_country(country, year)

        calc = SingleCountryCalculator(country, self.calculated_mean_data)
        calc.calculate_everything()
        self.complex_calculated_data = init_and_get_output_country_data_from_tuples(calc.output_values)


class CountryYearlyPredictedDataPack:
    def __init__(self, last_years_dict, year):
        class Placeholder:
            def __init__(self):
                pass

        self.last_years_dict = last_years_dict
        self.year = year
        self.calculated_mean_data = Placeholder()

        prev_years = last_years_dict.keys()
        for attribute in ATTRIBUTES:
            y_vals = list()

            for prev_year in prev_years:
                y_vals.append(getattr(last_years_dict[prev_year].calculated_mean_data, attribute))

            p = Predictor(YEARS_RANGE, y_vals, 2)
            setattr(self.calculated_mean_data, attribute, p.predict(year))


class CountryComparatorDataPack:
    class PredictedData:
        pass

    year_packs = dict()
    country_A = None
    country_B = None
    year2016 = PredictedData()

    def __init__(self, country_A, country_B):
        self.country_A = country_A
        self.country_B = country_B
        for year in YEARS_RANGE:
            new_data_pack = CountryComparatorYearlyDataPack(self.country_A, self.country_B, year)
            setattr(self, 'year' + str(year), new_data_pack)
            self.year_packs[year] = new_data_pack

        prev_years = self.year_packs.keys()
        for attribute in ['emigration_ratio', 'culture_ratio']:
            y_vals = list()

            for prev_year in prev_years:
                y_vals.append(getattr(self.year_packs[prev_year].comparator_calculated_data, attribute))

            p = Predictor(YEARS_RANGE, y_vals, 1)
            setattr(self.year2016, attribute, p.predict(2016))


class CountryComparatorYearlyDataPack:
    def __init__(self, country_A, country_B, year):
        self.year = year
        self.calculated_mean_data_A = get_mean_data_about_country(country_A, year)
        self.calculated_mean_data_B = get_mean_data_about_country(country_B, year)

        calc = TwoCountriesCalculator(country_A, self.calculated_mean_data_A, country_B, self.calculated_mean_data_B)
        calc.calculate_everything()
        self.comparator_calculated_data = init_and_get_output_country_data_from_tuples(calc.output_values)
