from data_management.data_manager import DataManager
from database_manager.models import Country


def test_data_manager():
    return DataManager().get_comparison_data_for_countries(
        Country.objects.first().id, Country.objects.last().id
    )


def test_data_pack():
    return DataManager().get_data_pack_for_country(
        Country.objects.first().id
    )
