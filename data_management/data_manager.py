from psychohistoria.properties import ATTRIBUTES
from .data_packs import CountryDataPack, CountryComparatorDataPack
from database_manager.models import Country


class DataManager:
    def __init__(self):
        pass

    def get_data_pack_for_country(self, country_id):
        country = Country.objects.get(id=country_id)
        cdp = CountryDataPack(country)
        return cdp

    def get_comparison_data_for_countries(self, country_A_id, country_B_id):
        country_A = Country.objects.get(id=country_A_id)
        country_B = Country.objects.get(id=country_B_id)
        cdp = CountryComparatorDataPack(country_A, country_B)
        return cdp

    def get_JS_arrays_for_charts(self, country_data_pack):
        class OutputPack:
            pass

        yearly_data = [
            country_data_pack.year2011.calculated_mean_data,
            country_data_pack.year2012.calculated_mean_data,
            country_data_pack.year2013.calculated_mean_data,
            country_data_pack.year2014.calculated_mean_data,
            country_data_pack.year2015.calculated_mean_data
        ]

        output = OutputPack()
        for attribute in ATTRIBUTES:
            setattr(output, attribute, '[')
            for data in yearly_data:
                val = getattr(output, attribute)
                val += str(getattr(data, attribute)) + ','
                setattr(output, attribute, val)
            with_last_comma = getattr(output, attribute)
            with_last_comma = list(with_last_comma)
            with_last_comma[len(with_last_comma) - 1] = ']'
            without_last_comma = "".join(with_last_comma)
            setattr(output, attribute, without_last_comma)

        return output

    def get_JS_arrays_for_prediction_charts(self, country_data_pack):
        class OutputPack:
            pass

        yearly_data = [
            country_data_pack.year2015.calculated_mean_data,
            country_data_pack.year2016.calculated_mean_data,
            country_data_pack.year2017.calculated_mean_data,
        ]

        output = OutputPack()
        for attribute in ATTRIBUTES:
            setattr(output, attribute, '[null, null, null, null,')
            for data in yearly_data:
                val = getattr(output, attribute)
                val += str(getattr(data, attribute)) + ','
                setattr(output, attribute, val)
            with_last_comma = getattr(output, attribute)
            with_last_comma = list(with_last_comma)
            with_last_comma[len(with_last_comma) - 1] = ']'
            without_last_comma = "".join(with_last_comma)
            setattr(output, attribute, without_last_comma)

        return output