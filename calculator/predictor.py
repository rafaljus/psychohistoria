from numpy import polyfit, poly1d


class Predictor:
    def __init__(self, x_args, y_vals, dim=2):
        self.x_args = x_args
        self.y_vals = y_vals
        coefficients = polyfit(self.x_args, self.y_vals, dim)
        self.poly = poly1d(coefficients)

    def predict(self, x_arg):
        return self.poly(x_arg)
