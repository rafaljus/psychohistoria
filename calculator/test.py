from .formulas import EmigrationRatioFormula

def test_emigration_ratio_formula():
    formula = EmigrationRatioFormula()
    print formula.variables
    formula.G = formula.m1 = formula.m2 = formula.m3 = formula.r = 2
    print formula.calculate()