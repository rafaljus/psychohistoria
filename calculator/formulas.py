import inspect


class BaseFormula(object):
    variables = list()

    def __init__(self):
        self.variables = dir(self)
        self._get_variables()

    def calculate(self):
        raise NotImplementedError()

    def _get_variables(self):
        attributes = inspect.getmembers(self.__class__, lambda a:not(inspect.isroutine(a)))
        self.variables = [a[0] for a in attributes if not(a[0].startswith('__') and a[0].endswith('__'))]
        self.variables.remove('variables')  # pozbycie sie petli dowiazan


class EmigrationRatioFormula(BaseFormula):
    G = m1 = m2 = m3 = r = 0

    def __init__(self):
        super(EmigrationRatioFormula, self).__init__()

    def calculate(self):
        return (self.G * self.m1 * self.m2 * self.m3) / (self.r ** 2)


class CultureRatioFormula(BaseFormula):
    G = m1 = m2 = r = 0

    def __init__(self):
        super(CultureRatioFormula, self).__init__()

    def calculate(self):
        return (self.G * self.m1 * self.m2) / (self.r ** 2)


class StayInCountryRatioFormula(BaseFormula):
    m1 = m2 = v1 = v2 = 0

    def __init__(self):
        super(StayInCountryRatioFormula, self).__init__()

    def calculate(self):
        return (self.m1 - self.m2)/(self.m1 + self.m2) * self.v1 + 2*self.m2/(self.m1 + self.m2) * self.v2


class LifeSatisfactionRatioFormula(BaseFormula):
    N = m = a = v = 0

    def __init__(self):
        super(LifeSatisfactionRatioFormula, self).__init__()

    def calculate(self):
        return (self.N * self.m / 3*self.a) * (self.v ** 2)
