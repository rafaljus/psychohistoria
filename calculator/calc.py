from calculator import formulas as f


class SingleCountryCalculator:
    def __init__(self, country, calculated_mean_data):  # jako argumenty przechodzi obiekt panstwa oraz wyliczone srednie
        self.output_values = list()
        self.country = country
        self.mean_data = calculated_mean_data

    def calculate_everything(self):
        self.calculate_life_satisfaction()
        self.calculate_stay_in_country_ratio()

    def calculate_life_satisfaction(self):
        formula = f.LifeSatisfactionRatioFormula()

        formula.N = self.mean_data.number_of_children
        formula.m = self.mean_data.size_of_house
        formula.a = self.mean_data.minutes_of_commuting
        formula.v = self.mean_data.health_index

        self.output_values.append(('life_satisfaction', formula.calculate()))

    def calculate_stay_in_country_ratio(self):
        formula = f.StayInCountryRatioFormula()

        formula.m1 = self.mean_data.government_support
        formula.v1 = self.mean_data.salary
        formula.m2 = self.mean_data.happiness
        formula.v2 = self.mean_data.health_index

        self.output_values.append(('stay_in_country', formula.calculate()))


class TwoCountriesCalculator:
    output_values = list()

    def __init__(self, country_A, calculated_mean_data_A, country_B, calculated_mean_data_B):  # jako argumenty przechodzi obiekt panstwa oraz wyliczone srednie
        self.country_A = country_A
        self.country_B = country_B
        self.mean_data_A = calculated_mean_data_A
        self.mean_data_B = calculated_mean_data_B

    def calculate_everything(self):
        self.calculate_emigration_ratio()
        self.calculate_culture_ratio()

    def calculate_emigration_ratio(self):
        formula = f.EmigrationRatioFormula()

        formula.m1 = self.mean_data_A.happiness
        formula.m2 = self.mean_data_A.salary
        formula.m3 = self.mean_data_A.government_support
        formula.G = self.mean_data_A.health_index
        formula.r = self.country_A.population
        F1 = formula.calculate()

        formula.m1 = self.mean_data_B.happiness
        formula.m2 = self.mean_data_B.salary
        formula.m3 = self.mean_data_B.government_support
        formula.G = self.mean_data_B.health_index
        formula.r = self.country_B.population
        F2 = formula.calculate()

        self.output_values.append(('emigration_ratio', F1/F2))

    def calculate_culture_ratio(self):
        formula = f.CultureRatioFormula()

        formula.m1 = self.mean_data_A.education
        formula.m2 = self.mean_data_A.salary
        formula.G = self.mean_data_A.is_female
        formula.r = self.mean_data_A.has_been_convicted
        F1 = formula.calculate()

        formula.m1 = self.mean_data_B.education
        formula.m2 = self.mean_data_B.salary
        formula.G = self.mean_data_B.is_female
        formula.r = self.mean_data_B.has_been_convicted
        F2 = formula.calculate()

        self.output_values.append(('culture_ratio', F1 / F2))