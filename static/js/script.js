$(document).ready(function () {
    var aCountryID;
    var bCountryID;

    var tempID;
    var btnCategory;

    $('.btn-select').click(function () {
        tempID = $(this).attr('id');
        btnCategory = tempID[tempID.length - 1];

        var $btnSelect = $('.btn-select');

        if (btnCategory == 'A') {
            aCountryID = tempID.substr(0, tempID.length - 1);
        }
        else {
            bCountryID = tempID.substr(0, tempID.length - 1);
        }

        if ($btnSelect.hasClass('btn-countries-selected') && btnCategory == 'A') {
            $('#select-2-countries-a .btn-countries-selected').removeClass('btn-countries-selected');
        }
        else if($btnSelect.hasClass('btn-countries-selected') && btnCategory == 'B')  {
            $('#select-2-countries-b .btn-countries-selected').removeClass('btn-countries-selected');
        }

        $(this).addClass('btn-countries-selected');


        if (btnCategory == 'A') {
            aCountryID = tempID.substr(0, tempID.length - 1);
        }
        else {
            bCountryID = tempID.substr(0, tempID.length - 1);
        }

    });

    $('#btn-compare').click(function () {
        if (aCountryID == bCountryID) {
            alert("Proszę wybrać dwa różne państwa");
        }
        else {
            if (aCountryID && bCountryID) {
                window.location.href = '/countries/' + aCountryID + '/' + bCountryID;
            }
            else alert("Proszę wybrać dwa państwa");
        }
    });
});

//function selectButtonCountry(this_id) {
//    if (($('#select-2-countries-a .btn-countries-selected').length > 1) && ($('#' + this_id).hasClass("btn-countries")))
//        return;
//    if ($('#' + this_id).hasClass("btn-countries")) {
//        $('#' + this_id).removeClass("btn-countries");
//        $('#' + this_id).addClass("btn-countries-selected");
//    } else {
//        $('#' + this_id).removeClass("btn-countries-selected");
//        $('#' + this_id).addClass("btn-countries");
//    }
//}
//function submitButtonCountry2() {
//    var $countriesSelectA = $('#select-2-countries-a .btn-countries-selected');
//    var $countriesSelectB = $('#select-2-countries-b .btn-countries-selected');
//
//    if ($countriesSelectA.length == 1 && $countriesSelectB.length == 1) {
//        var redirectUrl="countries";
//
//        $("#select-2-country :button").each(function () {
//            if ($('#' + this.id).hasClass("btn-countries-selected")) {
//                redirectUrl += "/" + this.id;
//            }
//        });
//        location.href = redirectUrl;
//    } else {
//        alert("Proszę, zaznacz dokładnie dwa państwa.");
//    }
//}
function selectButtonInfuence(this_id) {
    console.log("selectButtonInfuence" + this_id);
    if (($('#select-3-country .btn-countries-selected').length > 1) && ($('#' + this_id).hasClass("btn-countries")))
        return;
    if ($('#' + this_id).hasClass("btn-countries")) {
        $('#' + this_id).removeClass("btn-countries");
        $('#' + this_id).addClass("btn-countries-selected");
    } else {
        $('#' + this_id).removeClass("btn-countries-selected");
        $('#' + this_id).addClass("btn-countries");
    }
}
function submitButtonCountry3() {
    if ($('#select-3-country .btn-countries-selected').length == 2) {
        var redirectUrl = "countries";

        $("#select-3-country :button").each(function () {
            if ($('#' + this.id).hasClass("btn-countries-selected")) {
                redirectUrl += "/" + this.id;
            }
        });
        location.href = redirectUrl;
    } else {
        alert("Proszę, zaznacz dokładnie dwa państwa.");
    }
}