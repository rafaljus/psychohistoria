/**
 * Created by Agata on 10.06.2016.
 */

function createChart(id, dataTable){
    var data = {
      labels: ['2011', '2012', '2013', '2014', '2015'],
      series: [dataTable]
    };

    var options = {
        fullWidth: true,
        chartPadding: {
            right: 40
        },
        height: 250
    };

    return Chartist.Line(id, data, options);
}

function createWiderChart(id, dataTable, predictedTable){
    var data = {
      labels: ['2011', '2012', '2013', '2014', '2015', '2016', '2017'],
      series: [[], [], [], [], predictedTable, dataTable]
    };

    var options = {
        fullWidth: true,
        chartPadding: {
            right: 40
        },
        height: 250
    };

    return Chartist.Line(id, data, options);
}

function pleaseCreateWiderButSmallerChartOkay(id, dataTable, futureTable) {
    var data = {
      labels: ['2011', '2012', '2013', '2014', '2015', '2016'],
      series: [[], [], [], [], dataTable, futureTable]
    };

    var options = {
        fullWidth: true,
        chartPadding: {
            right: 40
        },
        height: 200
    };

    return Chartist.Line(id, data, options);
}