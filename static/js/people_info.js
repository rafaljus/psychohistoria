/**
 * Created by rafal_000 on 08.06.2016.
 */

var personID = null;
var year = null;

$(document).ready(function(){
    $('#select-year').hide();
    $('table').hide();
});

function getAjaxPeopleInfo(country_id) {
    $('table').show();
    var $people = $('#people_data_body');

    $people.hide();

    var getPeople = $.ajax({
        url: "/ajax/country/" + country_id + "/people",
        type: "GET",
        dataType: 'HTML'
    });

    getPeople.done(function (people_rows) {
        $people.html('');
        $people.append(
            people_rows
        );
    });

    showSexyAnimation($people);
}

function getAjaxPersonData(person_id, year) {
    var $personData = $('#person_data');

    $personData.hide();

    var getData = $.ajax({
        url: "/ajax/person/" + person_id + "/" + year,
        type: "GET",
        dataType: 'HTML'
    });

    getData.done(function (data) {
        $personData.html('');
        $personData.append(
            data
        );
    });

    goToTop();
    showSexyAnimation($personData);
}

function setPersonID(selected_id) {
    personID = selected_id;
    $('#person-id').html(personID);
    showSexyAnimation($('#select-year'));
    if (year) {
        getAjaxPersonData(personID, year);
    }
}

function setYearValue(selected_year) {
    year = selected_year;
    getAjaxPersonData(personID, year);
}

function showSexyAnimation($element) {
    $element.fadeIn('slow');
}

function goToTop() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
}