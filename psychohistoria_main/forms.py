from django import forms
from database_manager.models import Country


class CountryChoiceForm(forms.Form):
    country = forms.ChoiceField(
        choices=[(c.id, c.name)for c in Country.objects.all()],
        label='Wybierz kraj',
        required=True
    )