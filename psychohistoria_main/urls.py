from django.conf.urls import url

from psychohistoria_main import views

urlpatterns = [
    url(r'^$', views.index_view, name='index'),
    url(r'^index', views.index_view, name='index'),
    url(r'^country/(?P<country_id>[0-9]+)$', views.country_info_view, name='country-info'),
    url(r'^countries/(?P<country_A_id>[0-9]+)/(?P<country_B_id>[0-9]+)$', views.compare_countries_view, name='compare-countries'),
    url(r'^about', views.about_view, name='about'),
    url(r'^people/$', views.people_info_view, name='people-info'),
    url(r'^ajax/country/(?P<country_id>[0-9]+)/people$', views.ajax_get_people),
    url(r'^ajax/person/(?P<person_id>[0-9]+)/(?P<year>[0-9]+)/$', views.ajax_get_person_data)
    # url(r'^compare/(?P<country_id>[0-9]+)/(?P<country_id>[0-9]+)$', None, name='compare-countries')
]