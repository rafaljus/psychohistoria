from django import template

register = template.Library()


@register.filter
def percentage(value):
    if value < 0.1:
        return '< 1%'
    elif value > 0.99:
        return '> 99%'
    else:
        return format(value, ".2%")


@register.filter
def rounded(value):
    return format(value, ".2f")

