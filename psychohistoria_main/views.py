from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from data_management.data_manager import DataManager
from database_manager.models import Country, Person


def index_view(request):
    countries = Country.objects.all()
    return render(request, 'index.html', {'countries': countries})


def country_info_view(request, country_id):
    data_manager = DataManager()
    country_data = data_manager.get_data_pack_for_country(country_id)
    charts_data = data_manager.get_JS_arrays_for_charts(country_data)
    predicted_charts_data = data_manager.get_JS_arrays_for_prediction_charts(country_data)

    yearly_data = [
        country_data.year2011, country_data.year2012, country_data.year2013,
        country_data.year2014, country_data.year2015
    ]

    predicted_data = [
        country_data.year2016, country_data.year2017
    ]

    country = Country.objects.get(id=country_id)
    return render(request, 'country_info.html', {
        'country': country, 'yearly_data': yearly_data, 'predicted_data': predicted_data,
        'charts_data': charts_data, 'predicted_charts_data': predicted_charts_data
    })


def compare_countries_view(request, country_A_id, country_B_id):
    data_manager = DataManager()
    country_A_data = data_manager.get_data_pack_for_country(country_A_id)
    country_B_data = data_manager.get_data_pack_for_country(country_B_id)

    yearly_data_A = [
        country_A_data.year2011, country_A_data.year2012, country_A_data.year2013,
        country_A_data.year2014, country_A_data.year2015, country_A_data.year2016,
        country_A_data.year2017
    ]
    yearly_data_B = [
        country_B_data.year2011, country_B_data.year2012, country_B_data.year2013,
        country_B_data.year2014, country_B_data.year2015, country_B_data.year2016,
        country_B_data.year2017
    ]

    comparison_data = data_manager.get_comparison_data_for_countries(country_A_id, country_B_id)

    return render(request, 'compare_countries.html', {
        'country_A': Country.objects.get(id=country_A_id),
        'country_B': Country.objects.get(id=country_B_id),
        'yearly_data_A': yearly_data_A, 'yearly_data_B': yearly_data_B,
        'comparison_data': comparison_data
    })


def about_view(request):
    return render(request, 'about.html')


def people_info_view(request):
    countries = Country.objects.all()
    return render(request, 'people_info.html', {'countries': countries})


@csrf_exempt
def ajax_get_people(request, country_id):
    if request.is_ajax():
        country = Country.objects.get(id=country_id)
        output = country.get_all_citizens_as_html_table_rows()
        return HttpResponse(output)


@csrf_exempt
def ajax_get_person_data(request, person_id, year):
    if request.is_ajax():
        return HttpResponse(
            Person.objects.get(id=person_id).get_person_data_as_html_paragraphs(year)
        )
