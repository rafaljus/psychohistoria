class DatabaseGenerator:
    def __init__(self, country_generators_list):
        self.country_generators = country_generators_list

    def run(self):
        for country_generator in self.country_generators:
            country_generator().generate()


