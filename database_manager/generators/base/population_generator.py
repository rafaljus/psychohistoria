from random import random, choice

from database_manager.models import Person, PersonData
from database_manager.utils.normal_dist_random import NormalDistributionRandom
from psychohistoria.properties import YEARS_RANGE


class PopulationGenerator:
    def __init__(self, population_count, probabilities_dict, init_values_dict, cities):
        self.population_count = population_count
        self.probabilities_dict = probabilities_dict
        self.init_values_dict = init_values_dict
        self.cities = cities

        self.population = []
        self.population_index = 0

        self.init_new_person_data = None

    def generate(self):
        for _ in xrange(self.population_count):
            self._create_person()

    def get_population(self):
        return self.population

    def _create_person(self):
        person = Person()
        person = self._set_random_city(person)
        person.save()

        self.population_index += 1

        print("Tworzenie nowej osoby id:{} [{}/{}]".format(person.id, self.population_index, self.population_count))
        last_year_person_data = self._generate_initial_person_data(person.id, YEARS_RANGE[0])
        for year in YEARS_RANGE:
            print("  -  dane dla roku: {}".format(year))
            last_year_person_data = self._generate_another_year_person_data(last_year_person_data, person.id, year)

    def _generate_initial_person_data(self, related_person_id, data_year):
        person_data = PersonData()

        person_data = self._initialize_binary_fields(person_data)
        person_data = self._initialize_nominal_fields(person_data)
        person_data = self._initialize_integer_fields(person_data)
        person_data = self._initialize_float_fields(person_data)
        person_data = self._initialize_complex_fields(person_data)

        person_data.related_person_id = related_person_id
        person_data.year = data_year

        return person_data

    def _initialize_binary_fields(self, person_data):
        person_data.is_employed = self._set_random_binary_value(
            self.probabilities_dict['is_employed']
        )
        person_data.is_female = self._set_random_binary_value(
            self.probabilities_dict['is_female']
        )
        person_data.has_been_convicted = self._set_random_binary_value(
            self.probabilities_dict['has_been_convicted']
        )
        person_data.is_addicted_to_alcohol = self._set_random_binary_value(
            self.probabilities_dict['is_addicted_to_alcohol']
        )
        person_data.is_a_smoker = self._set_random_binary_value(
            self.probabilities_dict['is_a_smoker']
        )
        person_data.has_heart_issues = self._set_random_binary_value(
            self.probabilities_dict['has_heart_issues']
        )
        person_data.is_addicted_to_drugs = self._set_random_binary_value(
            self.probabilities_dict['is_addicted_to_drugs']
        )
        person_data.is_active = self._set_random_binary_value(
            self.probabilities_dict['is_active']
        )
        person_data.healthy_food = self._set_random_binary_value(
            self.probabilities_dict['healthy_food']
        )
        person_data.has_cancer = self._set_random_binary_value(
            self.probabilities_dict['has_cancer']
        )
        person_data.is_disabled = self._set_random_binary_value(
            self.probabilities_dict['is_disabled']
        )
        person_data.is_homeless = self._set_random_binary_value(
            self.probabilities_dict['is_homeless']
        )
        return person_data

    def _initialize_nominal_fields(self, person_data):
        person_data.education = self._set_random_integer_value(
            0, self.init_values_dict['education'], 4
        )
        person_data.happiness = self._set_random_integer_value(
            1, self.init_values_dict['happiness'], 10
        )
        person_data.government_support = self._set_random_integer_value(
            1, self.init_values_dict['government_support'], 10
        )
        return person_data

    def _initialize_integer_fields(self, person_data):
        """

        :type person_data: object
        """
        person_data.age = self._set_random_integer_value(
            16, self.init_values_dict['age'], 100
        )
        person_data.number_of_children = self._set_random_integer_value(
            0, self.init_values_dict['number_of_children'], 10
        )
        person_data.size_of_house = self._set_random_integer_value(
            15, self.init_values_dict['size_of_house'], 500
        ) if not person_data.is_homeless else 0
        person_data.minutes_of_commuting = self._set_random_integer_value(
            0, self.init_values_dict['minutes_of_commuting'], 120
        )
        person_data.salary = self._set_random_integer_value(
            1000, self.init_values_dict['salary'], 30000
        ) if person_data.is_employed else 0
        return person_data

    def _initialize_float_fields(self, person_data):
        person_data.distance_from_health_care = self._set_random_float_value(
            0, self.init_values_dict['distance_from_health_care'], 15000
        )
        return person_data

    def _initialize_complex_fields(self, person_data):
        person_data.health_index = 100 - ((100 - person_data.age)*0.3 + person_data.is_disabled*5 +
            person_data.is_addicted_to_alcohol*5 + person_data.is_a_smoker*10 + person_data.has_heart_issues*10 +
            person_data.is_addicted_to_drugs*15 + (person_data.distance_from_health_care/15000)*5 +
            random()*20) - (person_data.is_active*10 + person_data.healthy_food*5)

        return person_data

    def _set_random_binary_value(self, probability):
        return probability > random()

    def _set_random_integer_value(self, min_value, medium_value, max_value):
        dist = NormalDistributionRandom(min_value, medium_value, max_value)
        return dist.get_value()

    def _set_random_float_value(self, min_value, medium_value, max_value):
        dist = NormalDistributionRandom(min_value, medium_value, max_value)
        return dist.get_value(integer=False)

    def _set_random_city(self, person):
        person.city = choice(self.cities)
        return person

    def _generate_another_year_person_data(self, last_year_person_data, related_person_id, year):
        person_data = PersonData()

        person_data.is_female = last_year_person_data.is_female

        person_data.age = last_year_person_data.age + 1

        if person_data.age > 16:
            person_data.is_employed = self._set_random_binary_value(
                self.probabilities_dict['is_employed']
            )
        else:
            person_data.is_employed = 0

        person_data.is_homeless = self._set_random_binary_value(
            self.probabilities_dict['is_homeless']
        )

        person_data.has_been_convicted = self._set_random_binary_value(
            self.probabilities_dict['has_been_convicted']
        ) if not last_year_person_data.has_been_convicted else True
        person_data.has_heart_issues = self._set_random_binary_value(
            self.probabilities_dict['has_heart_issues']
        ) if not last_year_person_data.has_heart_issues else True
        person_data.has_cancer = self._set_random_binary_value(
            self.probabilities_dict['has_cancer']
        ) if not last_year_person_data.has_cancer else True
        person_data.is_disabled = self._set_random_binary_value(
            self.probabilities_dict['is_disabled']
        ) if not last_year_person_data.is_disabled else True

        person_data.is_addicted_to_alcohol = self._set_random_binary_value(
            self.probabilities_dict['is_addicted_to_alcohol']
        )
        person_data.is_a_smoker = self._set_random_binary_value(
            self.probabilities_dict['is_a_smoker']
        )
        person_data.is_addicted_to_drugs = self._set_random_binary_value(
            self.probabilities_dict['is_addicted_to_drugs']
        )
        person_data.is_active = self._set_random_binary_value(
            self.probabilities_dict['is_active']
        )
        person_data.healthy_food = self._set_random_binary_value(
            self.probabilities_dict['healthy_food']
        )

        education = self._set_random_integer_value(
            0, self.init_values_dict['education'], 4
        )
        person_data.education = \
            education if education > last_year_person_data.education else last_year_person_data.education
        number_of_children = self._set_random_integer_value(
            0, self.init_values_dict['number_of_children'], 10
        )
        person_data.number_of_children = \
            number_of_children if number_of_children > last_year_person_data.number_of_children else last_year_person_data.number_of_children
        person_data.size_of_house = self._set_random_integer_value(
            0, self.init_values_dict['size_of_house'], 500
        )
        person_data.happiness = self._set_random_integer_value(
            1, self.init_values_dict['happiness'], 10
        )
        person_data.government_support = self._set_random_integer_value(
            1, self.init_values_dict['government_support'], 10
        )
        person_data.minutes_of_commuting = self._set_random_integer_value(
            0, self.init_values_dict['minutes_of_commuting'], 120
        )

        person_data.salary = self._set_random_integer_value(
            200, self.init_values_dict['salary'], 30000
        ) if person_data.is_employed else 0
        person_data.distance_from_health_care = self._set_random_float_value(
            20, self.init_values_dict['distance_from_health_care'], 15000
        )

        person_data.health_index = 100 - ((100 - person_data.age) * 0.3 + person_data.is_disabled * 5 +
                                          person_data.is_addicted_to_alcohol * 5 + person_data.is_a_smoker * 10 + person_data.has_heart_issues * 10 +
                                          person_data.is_addicted_to_drugs * 15 + (
                                          person_data.distance_from_health_care / 15000) * 5 +
                                          random() * 20) - (person_data.is_active * 10 + person_data.healthy_food * 5)

        person_data.related_person_id = related_person_id
        person_data.year = year
        person_data.save()

        return person_data
