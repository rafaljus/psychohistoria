from database_manager.models import City


class CitiesGenerator:
    def __init__(self, cities_names_list):
        self.cities = []
        self.cities_names = cities_names_list

    def generate(self):
        for city_name in self.cities_names:
            new_city = City()
            new_city.name = city_name
            self.cities.append(new_city)

    def get_cities(self):
        return self.cities
