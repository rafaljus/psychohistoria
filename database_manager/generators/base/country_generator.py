from .cities_generator import CitiesGenerator
from .population_generator import PopulationGenerator
from ...models import Country


class BaseCountryDataGenerator(object):
    # Country properties
    name = ''
    population_count = 0
    cities_names = []

    # Values for random population generator
    probability_dict = {
        'is_employed': 0.01,
        'is_female': 0.01,
        'has_been_convicted': 0.01,
        'is_addicted_to_alcohol': 0.01,
        'is_a_smoker': 0.01,
        'has_heart_issues': 0.01,
        'is_addicted_to_drugs': 0.01,
        'is_active': 0.01,
        'healthy_food': 0.01,
        'has_cancer': 0.01,
        'is_homeless': 0.01
    }

    base_values_dict = {
        'age': 30,
        'number_of_children': 0,
        'size_of_house': 0,
        'education': 0,
        'happiness': 1,
        'government_support': 1,
        'minutes_of_commuting': 1,
        'distance_from_health_care': 1.0,
        'salary': 1000
    }

    # Output from generators
    _country = None
    _cities = []
    _population = []

    # Generators
    _cities_generator = None
    _population_generator = None

    def __init__(self):
        self._initialize_cities_generator()

    def generate(self):
        self._generate_country()
        self._generate_cities()

        self._initialize_population_generator()
        self._generate_population()

    def _initialize_cities_generator(self):
        self._cities_generator = CitiesGenerator(self.cities_names)
        self._population_generator = PopulationGenerator(self.population_count, self.probability_dict,
                                                         self.base_values_dict, self._cities)

    def _initialize_population_generator(self):
        self._cities_generator = CitiesGenerator(self.cities_names)
        self._population_generator = PopulationGenerator(self.population_count, self.probability_dict,
                                                         self.base_values_dict, self._cities)

    def _generate_country(self):
        print "\nGenerowanie panstwa {}".format(self.name)
        self._country = Country()
        self._country.name = self.name
        self._country.save()

    def _generate_cities(self):
        print "Generowanie miast {}".format(self.cities_names)
        self._cities_generator.generate()
        self._cities = self._cities_generator.get_cities()
        for city in self._cities:
            city.country = self._country
            city.save()

    def _generate_population(self):
        print "Generowanie populacji"
        self._population_generator.generate()
        self._population = self._population_generator.get_population()
        # TODO
