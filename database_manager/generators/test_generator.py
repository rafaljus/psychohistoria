from database_manager.generators.base.country_generator import BaseCountryDataGenerator


class TestCountryGenerator(BaseCountryDataGenerator):
    def __init__(self):
        self.name = 'Test Country'
        self.population_count = 10
        self.cities_names = [
            'Big Test City',
            'Another Test City'
        ]

        self.probability_dict = {
            'is_employed': 0.5,
            'is_disabled': 0.5,
            'is_female': 0.5,
            'has_been_convicted': 0.5,
            'is_addicted_to_alcohol': 0.5,
            'is_a_smoker': 0.5,
            'has_heart_issues': 0.5,
            'is_addicted_to_drugs': 0.5,
            'is_active': 0.5,
            'healthy_food': 0.5,
            'has_cancer': 0.5,
            'is_homeless': 0.5
        }

        self.base_values_dict = {
            'age': 30,
            'number_of_children': 2,
            'size_of_house': 30,
            'education': 1,
            'happiness': 5,
            'government_support': 5,
            'minutes_of_commuting':  20,
            'distance_from_health_care': 1000.0,
            'salary': 2000
        }

        super(TestCountryGenerator, self).__init__()


