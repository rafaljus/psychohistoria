from database_manager.generators.base.country_generator import BaseCountryDataGenerator


class NewIndiaGenerator(BaseCountryDataGenerator):
    def __init__(self):
        self.name = 'New India'
        self.population_count = 200
        self.cities_names = [
            'Bumbaj City',
            'Sequin City',
            'Tiger City',
            'Elephant City'

        ]

        self.probability_dict = {
            'is_employed': 0.4,
            'is_disabled': 0.3,
            'is_female': 0.7,
            'has_been_convicted': 0.2,
            'is_addicted_to_alcohol': 0.4,
            'is_a_smoker': 0.4,
            'has_heart_issues': 0.5,
            'is_addicted_to_drugs': 0.4,
            'is_active': 0.6,
            'healthy_food': 0.2,
            'has_cancer': 0.3,
            'is_homeless': 0.5
        }

        self.base_values_dict = {
            'age': 27,
            'number_of_children': 8,
            'size_of_house': 20,
            'education': 0,
            'happiness': 3,
            'government_support': 3,
            'minutes_of_commuting':  90,
            'distance_from_health_care': 13000.0,
            'salary': 1000
        }

        super(NewIndiaGenerator, self).__init__()


