from database_manager.generators.base.country_generator import BaseCountryDataGenerator


class FarmVilleGenerator(BaseCountryDataGenerator):
    def __init__(self):
        self.name = 'Farm Ville'
        self.population_count = 160
        self.cities_names = [
            'Cow City',
            'Horse City',
            'Tractor City'
        ]

        self.probability_dict = {
            'is_employed': 0.8,
            'is_disabled': 0.01,
            'is_female': 0.5,
            'has_been_convicted': 0.1,
            'is_addicted_to_alcohol': 0.3,
            'is_a_smoker': 0.5,
            'has_heart_issues': 0.3,
            'is_addicted_to_drugs': 0.06,
            'is_active': 0.9,
            'healthy_food': 0.8,
            'has_cancer': 0.1,
            'is_homeless': 0.1
        }

        self.base_values_dict = {
            'age': 32,
            'number_of_children': 4,
            'size_of_house': 100,
            'education': 2,
            'happiness': 7,
            'government_support': 6,
            'minutes_of_commuting':  45,
            'distance_from_health_care': 5000.0,
            'salary': 2500
        }

        super(FarmVilleGenerator, self).__init__()


