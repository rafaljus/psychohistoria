from database_manager.generators.base.country_generator import BaseCountryDataGenerator


class FitlandGenerator(BaseCountryDataGenerator):
    def __init__(self):
        self.name = 'Fitland'
        self.population_count = 140
        self.cities_names = [
            'Stretching City',
            'Kardio City',
            'Muscle City',
            'Crossfit City'
        ]

        self.probability_dict = {
            'is_employed': 0.8,
            'is_disabled': 0.02,
            'is_female': 0.6,
            'has_been_convicted': 0.05,
            'is_addicted_to_alcohol': 0.1,
            'is_a_smoker': 0.05,
            'has_heart_issues': 0.05,
            'is_addicted_to_drugs': 0.1,
            'is_active': 0.9,
            'healthy_food': 0.8,
            'has_cancer': 0.05,
            'is_homeless': 0.01
        }

        self.base_values_dict = {
            'age': 25,
            'number_of_children': 1,
            'size_of_house': 50,
            'education': 4,
            'happiness': 7,
            'government_support': 4,
            'minutes_of_commuting':  15,
            'distance_from_health_care': 1500.0,
            'salary': 3500
        }

        super(FitlandGenerator, self).__init__()


