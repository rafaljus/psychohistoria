from database_manager.generators.base.country_generator import BaseCountryDataGenerator


class LosDevilosGenerator(BaseCountryDataGenerator):
    def __init__(self):
        self.name = 'Los Devilos'
        self.population_count = 130
        self.cities_names = [
            'Burning City',
            'Ugly City',
            'Fallen City'
        ]

        self.probability_dict = {
            'is_employed': 0.2,
            'is_disabled': 0.3,
            'is_female': 0.2,
            'has_been_convicted': 0.8,
            'is_addicted_to_alcohol': 0.7,
            'is_a_smoker': 0.6,
            'has_heart_issues': 0.4,
            'is_addicted_to_drugs': 0.7,
            'is_active': 0.5,
            'healthy_food': 0.5,
            'has_cancer': 0.3,
            'is_homeless': 0.4
        }

        self.base_values_dict = {
            'age': 40,
            'number_of_children': 1,
            'size_of_house': 30,
            'education': 3,
            'happiness': 2,
            'government_support': 1,
            'minutes_of_commuting':  60,
            'distance_from_health_care': 3000.0,
            'salary': 1200
        }

        super(LosDevilosGenerator, self).__init__()


