from database_manager.generators.base.country_generator import BaseCountryDataGenerator


class KebabostanGenerator(BaseCountryDataGenerator):
    def __init__(self):
        self.name = 'Kebabostan'
        self.population_count = 150
        self.cities_names = [
            'Salad City',
            'Mutton City',
            'Chicken City',
            'Sauce City'
        ]

        self.probability_dict = {
            'is_employed': 0.7,
            'is_disabled': 0.3,
            'is_female': 0.5,
            'has_been_convicted': 0.6,
            'is_addicted_to_alcohol': 0.6,
            'is_a_smoker': 0.6,
            'has_heart_issues': 0.3,
            'is_addicted_to_drugs': 0.15,
            'is_active': 0.3,
            'healthy_food': 0.1,
            'has_cancer': 0.1,
            'is_homeless': 0.2
        }

        self.base_values_dict = {
            'age': 29,
            'number_of_children': 5,
            'size_of_house': 20,
            'education': 1,
            'happiness': 6,
            'government_support': 7,
            'minutes_of_commuting':  30,
            'distance_from_health_care': 1000.0,
            'salary': 1000
        }

        super(KebabostanGenerator, self).__init__()


