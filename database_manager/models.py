from django.db import models

from html_utils.html_builders import ParagraphBuilder


class Country(models.Model):
    name = models.CharField(max_length=50)

    def get_all_citizens(self):
        return Person.objects.filter(city__country_id=self.id)

    def get_all_citizens_data(self):
        return PersonData.objects.filter(related_person__city__country_id=self.id)

    def get_all_citizens_data_from_year(self, year):
        return PersonData.objects.filter(related_person__city__country_id=self.id, year=year)

    def get_all_citizens_as_html_table_rows(self):
        people = self.get_all_citizens()
        output = str()
        for person in people:
            output += person.get_person_as_html_table_row()
        return output

    @property
    def population(self):
        return Person.objects.filter(city__country_id=self.id).count()

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name_plural = 'Countries'


class City(models.Model):
    country = models.ForeignKey(Country, related_name='cities')
    name = models.CharField(max_length=50)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name_plural = 'Cities'


class Person(models.Model):
    city = models.ForeignKey(City, related_name='people')

    def get_data_from_year(self, year):
        try:
            return PersonData.objects.get(related_person_id=self.id, year=year)
        except:
            print 'ERROR: No data for this year'

    def get_person_as_html_table_row(self):
        output = '<tr class="person-row" id="{}" onclick="setPersonID({});"><td>'.format(self.id, self.id)
        output += str(self.id)
        output += '</td><td>'
        output += self.city.name
        output += '</td></tr>'
        return output

    def get_person_data_as_html_paragraphs(self, year):
        data = self.get_data_from_year(year)
        pb = ParagraphBuilder()
        output = '<h3><strong>Rok {}</strong></h3>'.format(year)
        output += pb.build('Wiek', data.age)
        output += pb.build('Zatrudniony', data.is_employed)
        output += pb.build('Bezdomny', data.is_homeless)
        output += pb.build('Czy kobieta', data.is_female)
        output += pb.build('Niepelnosprawny', data.is_disabled)
        output += pb.build('Karany', data.has_been_convicted)
        output += pb.build('Alkoholizm', data.is_addicted_to_alcohol)
        output += pb.build('Pali papierosy', data.is_a_smoker)
        output += pb.build('Problemy z sercem', data.has_heart_issues)
        output += pb.build('Uzaleznienie od narkotykow', data.is_addicted_to_drugs)
        output += pb.build('Aktywnosc fizyczna', data.is_active)
        output += pb.build('Zdrowa dieta', data.healthy_food)
        output += pb.build('Wyksztalcenie', data.education, 4)
        output += pb.build('Zadowolenie z zycia', data.happiness, 10)
        output += pb.build('Poparcie dla rzadu', data.government_support, 10)
        output += pb.build('Liczba dzieci', data.number_of_children)
        output += pb.build('Rozmiar domu', data.size_of_house)
        output += pb.build('Minuty dojazdu do pracy', data.minutes_of_commuting)
        output += pb.build('Pensja', data.salary)
        output += pb.build('Odleglosc od osrodka zdrowia', data.distance_from_health_care)
        output += pb.build('Wyznacznik zdrowia', data.health_index)
        return output

    def print_data(self):
        print "\nOSOBA O NUMERZE ID: {}".format(self.id)
        print "Obywatel miasta {} w panstwie {}".format(self.city.name, self.city.country.name)
        print "-------------------------------------"

        person_data = PersonData.objects.filter(related_person_id=self.id)
        attributes = [
            'is_employed', 'is_disabled', 'is_female', 'has_been_convicted',
            'is_addicted_to_alcohol', 'is_a_smoker', 'has_heart_issues',
            'is_addicted_to_drugs', 'is_active', 'healthy_food', 'has_cancer',
            'is_homeless', 'education', 'happiness', 'government_support', 'age',
            'number_of_children', 'size_of_house', 'minutes_of_commuting',
            'salary', 'distance_from_health_care', 'health_index'
        ]

        for data in person_data:
            print "\nW roku: {}".format(data.year)
            print "------------"

            for attribute in attributes:
                if attribute[0] != '_':
                    print "{}: {}".format(
                        attribute, getattr(data, attribute)
                    )

    def __str__(self):
        return unicode("{} Obywatel {} miasta {}".format(self.id, self.city.country.name, self.city.name))

    class Meta:
        verbose_name_plural = 'People'


class PersonData(models.Model):
    related_person = models.ForeignKey(Person, unique=False)
    year = models.IntegerField()

    # Binary:
    is_employed = models.BooleanField()
    is_disabled = models.BooleanField()
    is_female = models.BooleanField()
    has_been_convicted = models.BooleanField()
    is_addicted_to_alcohol = models.BooleanField()
    is_a_smoker = models.BooleanField()
    has_heart_issues = models.BooleanField()
    is_addicted_to_drugs = models.BooleanField()
    is_active = models.BooleanField()
    healthy_food = models.BooleanField()
    has_cancer = models.BooleanField()
    is_homeless = models.BooleanField()

    # Nominal:
    education = models.IntegerField()  # 0=brak; 1=podstawowe; 2=zawodowe; 3=srednie; 4=wyzsze
    happiness = models.IntegerField()  # 1-10
    government_support = models.IntegerField()  # 1-10

    # Integers:
    age = models.IntegerField()
    number_of_children = models.IntegerField()
    size_of_house = models.IntegerField()
    minutes_of_commuting = models.IntegerField()
    salary = models.IntegerField()

    # Floats:
    distance_from_health_care = models.FloatField()

    # Complex attributes:
    health_index = models.FloatField()

    def __str__(self):
        return unicode("[{}] {} Obywatel {} miasta {}".format(self.year, self.related_person.id,
                                                              self.related_person.city.country.name,
                                                              self.related_person.city.name))
