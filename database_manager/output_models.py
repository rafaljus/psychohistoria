def init_and_get_output_country_data(values_dict):
    output = OutputCountryData()
    keys = values_dict.keys()
    for key in keys:
        setattr(output, key, values_dict[key])

    return output

def init_and_get_output_country_data_from_tuples(list_of_tuples):
    output = OutputCountryData()
    for tuple in list_of_tuples:
        setattr(output, tuple[0], tuple[1])
    return output


class OutputCountryData:
    pass  # najwieksza klasa ever