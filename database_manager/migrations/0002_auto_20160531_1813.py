# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('database_manager', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='persondata',
            name='attachment_to_the_country',
        ),
        migrations.RemoveField(
            model_name='persondata',
            name='life_satisfaction',
        ),
    ]
