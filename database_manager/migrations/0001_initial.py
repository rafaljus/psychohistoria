# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Countries',
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.ForeignKey(related_name='people', to='database_manager.City')),
            ],
            options={
                'verbose_name_plural': 'People',
            },
        ),
        migrations.CreateModel(
            name='PersonData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField()),
                ('is_employed', models.BooleanField()),
                ('is_disabled', models.BooleanField()),
                ('is_female', models.BooleanField()),
                ('has_been_convicted', models.BooleanField()),
                ('is_addicted_to_alcohol', models.BooleanField()),
                ('is_a_smoker', models.BooleanField()),
                ('has_heart_issues', models.BooleanField()),
                ('is_addicted_to_drugs', models.BooleanField()),
                ('is_active', models.BooleanField()),
                ('healthy_food', models.BooleanField()),
                ('has_cancer', models.BooleanField()),
                ('education', models.IntegerField()),
                ('happiness', models.IntegerField()),
                ('age', models.IntegerField()),
                ('number_of_children', models.IntegerField()),
                ('size_of_house', models.IntegerField()),
                ('minutes_of_commuting', models.IntegerField()),
                ('salary', models.IntegerField()),
                ('distance_from_health_care', models.FloatField()),
                ('life_satisfaction', models.IntegerField()),
                ('health_index', models.FloatField()),
                ('attachment_to_the_country', models.FloatField()),
                ('related_person', models.OneToOneField(to='database_manager.Person')),
            ],
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(related_name='cities', to='database_manager.Country'),
        ),
    ]
