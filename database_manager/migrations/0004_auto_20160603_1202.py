# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('database_manager', '0003_auto_20160531_1827'),
    ]

    operations = [
        migrations.AddField(
            model_name='persondata',
            name='government_support',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='persondata',
            name='is_homeless',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
