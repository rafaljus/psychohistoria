# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('database_manager', '0002_auto_20160531_1813'),
    ]

    operations = [
        migrations.AlterField(
            model_name='persondata',
            name='related_person',
            field=models.ForeignKey(to='database_manager.Person'),
        ),
    ]
