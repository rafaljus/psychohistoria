# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from database_manager.generator import generate_database
from database_manager.destroyer import destroy_all_records


class Command(BaseCommand):
    def handle(self, *args, **options):
        print u"Czy chcesz wygenerować nową bazę? Stara baza zostanie nadpisana [T/n]"
        if raw_input().lower() == 't':
            destroy_all_records()
            generate_database()
        else:
            print u"Proces tworzenia bazy został przerwany"
