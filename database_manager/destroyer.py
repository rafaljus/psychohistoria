from .models import Person, City, Country, PersonData


def destroy_all_records():
    models = (Person, City, Country, PersonData)
    for model in models:
        print 'Kasowanie obiektow klasy {}'.format(model.__name__)
        for ob in model.objects.all():
            ob.delete()

