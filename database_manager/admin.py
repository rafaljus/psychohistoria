from django.contrib import admin
from .models import City, Country, Person, PersonData

admin.site.register(City)
admin.site.register(Country)
admin.site.register(Person)
admin.site.register(PersonData)
