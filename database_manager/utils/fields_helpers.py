from database_manager.models import PersonData

def get_model_field_names(model_class):
    return model_class._meta.get_all_field_names()

def get_person_data_fields():
    return get_model_field_names(PersonData)