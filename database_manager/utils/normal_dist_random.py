from numpy.random import normal
from numpy import around

# TODO: Poprawic ten modul


class NormalDistributionRandom:
    def __init__(self, min_val, mean, max_val):
        self.min_val = min_val
        self.mean = mean
        self.max_val = max_val

    def get_value(self, integer=True):
        width_of_distribution = self._get_width_of_normal_distribution()
        out_val = normal(self.mean, width_of_distribution)
        return self._normalize_value(out_val, integer)

    def _get_width_of_normal_distribution(self):
        width_a = self.max_val - self.mean
        width_b = self.mean - self.min_val

        N = 2

        return width_a / N if width_a > width_b else width_b / N  # dzielone przez N aby szerokosc dawala wyniki blizsze sredniej

    def _normalize_value(self, val, to_integer=True):
        if val > self.max_val:
            return self.max_val
        elif val < self.min_val:
            return self.min_val
        else:
            return around(val, 0) if to_integer else val
