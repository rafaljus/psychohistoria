from database_manager.aggregators import MeanCalculator
from output_models import init_and_get_output_country_data

# Jedyny modul ktory sie liczy.

def get_mean_data_about_country(country, year):
    calc = MeanCalculator(country)
    calc.calculate_for_year(year)
    return init_and_get_output_country_data(calc.calculated_data)


def test():
    from database_manager.models import Country
    return get_mean_data_about_country(Country.objects.first(), 2015)