from .generators.database_generator import DatabaseGenerator
from .generators.test_generator import TestCountryGenerator
from .generators.countries.farmville_generator import FarmVilleGenerator
from .generators.countries.fitland_generator import FitlandGenerator
from .generators.countries.kebabostan_generator import KebabostanGenerator
from .generators.countries.los_devilos_generator import LosDevilosGenerator
from .generators.countries.new_india_generator import NewIndiaGenerator

def generate_database():
    # UWAGA: zostawic odkomentowana tylko jedna opcje

    # Tworzenie testowej bazy danych malego jednego kraju:
    # generate_test_database()

    # Tworzenie bazy roznych panstw:
    generate_countries_database()


def generate_test_database():
    print "TWORZENIE TESTOWEJ BAZY DANYCH\n"
    country_generators = [
        TestCountryGenerator
    ]
    DatabaseGenerator(country_generators).run()


def generate_countries_database():
    print "TWORZENIE WLASCIWEJ BAZY DANYCH\n"
    country_generators = [
        FarmVilleGenerator,
        FitlandGenerator,
        KebabostanGenerator,
        LosDevilosGenerator,
        NewIndiaGenerator
    ]
    DatabaseGenerator(country_generators).run()
