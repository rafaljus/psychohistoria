from django.db.models import Avg, Max

from database_manager.utils.fields_helpers import get_person_data_fields


class BaseAggregator(object):
    _AGGREGATOR = None  # klasa agregatora z pakietu django.db.models
    _FIELDS = get_person_data_fields()

    def __init__(self, country_instance):
        self.country_instance = country_instance
        self.calculated_data = dict()

    def calculate_for_year(self, year):
        for field_name in self._FIELDS:
            data_queryset = \
                self.country_instance.get_all_citizens_data_from_year(year).aggregate(
                    avg=self._AGGREGATOR(field_name)
                )
            self.calculated_data[field_name] = data_queryset['avg']

    def print_calculated_data(self):
        keys = self.calculated_data.keys()
        print '{}'.format(self.__class__)
        for key in keys:
            print '{}: {}'.format(
                key, self.calculated_data[key]
            )


class MeanCalculator(BaseAggregator):
    _AGGREGATOR = Avg

    def __init__(self, country_instance):
        super(BaseAggregator, self).__init__()
        self.country_instance = country_instance
        self.calculated_data = dict()


class MedianCalculator(BaseAggregator):
    _AGGREGATOR = None  # TODO odkryc jak to zrobic

    def __init__(self, country_instance):
        super(BaseAggregator, self).__init__()
        self.country_instance = country_instance
        self.calculated_data = dict()


class MaxCalculator(BaseAggregator):
    _AGGREGATOR = Max

    def __init__(self, country_instance):
        super(BaseAggregator, self).__init__()
        self.country_instance = country_instance
        self.calculated_data = dict()